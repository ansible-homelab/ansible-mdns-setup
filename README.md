# Ansible mDNS Setup

Ansible playbook to set up mDNS on any system with systemd and specifically systemd-resolved (so without using avahi).

This was tested on Ubuntu 20.04 LTS but should work any distribution with systemd (which is almost all distributions these days)
